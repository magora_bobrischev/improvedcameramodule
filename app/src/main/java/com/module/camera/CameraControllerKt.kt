package com.module.camera

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.graphics.SurfaceTexture
import android.hardware.Camera
import android.media.MediaRecorder
import android.media.ThumbnailUtils
import android.os.Build
import android.provider.MediaStore
import java.io.File
import java.io.FileOutputStream
import java.util.*
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.Semaphore
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit
import kotlin.experimental.and

@Suppress("DEPRECATION")
/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 18.02.18.
 */
object CameraControllerKt : MediaRecorder.OnInfoListener {
    private const val CORE_POOL_SIZE = 1
    private const val MAX_POOL_SIZE = 1
    private const val KEEP_ALIVE_SECONDS = 60
    private const val PHOTO_SIZE = 1920
    private val threadPool: ThreadPoolExecutor by lazy {
        ThreadPoolExecutor(CORE_POOL_SIZE, MAX_POOL_SIZE, KEEP_ALIVE_SECONDS.toLong(), TimeUnit.SECONDS, LinkedBlockingQueue())
    }
    val availableFlashModes = ArrayList<String>()
    private var recorder: MediaRecorder? = null
    private var recordedFilePath: String? = null
    private var recordedFile: File? = null
    var cameraInfos: ArrayList<CameraInfo>? = null
    var cameraInitCallback: (() -> Unit)? = null
    private var onVideoTakeCallback: ((thumb: Bitmap?, recordedFile: File?) -> Unit)? = null
    private var recordingSmallVideo: Boolean = false
    private var cameraInitied: Boolean = false
    private var loadingCameras: Boolean = false
    val isCameraInited
        get() = cameraInitied && cameraInfos != null && !cameraInfos!!.isEmpty()

    fun initCamera() {
        if (loadingCameras || cameraInitied) {
            return
        }
        loadingCameras = true
        threadPool.execute {
            try {
                if (cameraInfos == null) {
                    val count = Camera.getNumberOfCameras()
                    val result = ArrayList<CameraInfo>()
                    val info = Camera.CameraInfo()

                    for (cameraId in 0 until count) {
                        Camera.getCameraInfo(cameraId, info)
                        val cameraInfo = CameraInfo(cameraId, info)

                        val camera = Camera.open(cameraInfo.cameraId)
                        val params = camera.parameters

                        var list: List<Camera.Size> = params.supportedPreviewSizes
                        list.filter { !(it.width == 1280 && it.height != 720) && it.height < 2160 && it.width < 2160 }
                                .forEach { cameraInfo.previewSizes.add(Size(it.width, it.height)) }

                        list = params.supportedPictureSizes
                        list.filter { !(it.width == 1280 && it.height != 720) && ("samsung" != Build.MANUFACTURER || "jflteuc" != Build.PRODUCT || it.width < 2048) }
                                .forEach { cameraInfo.pictureSizes.add(Size(it.width, it.height)) }

                        camera.release()
                        result.add(cameraInfo)
                        Collections.sort(cameraInfo.previewSizes)
                        Collections.sort(cameraInfo.pictureSizes)
                    }
                    cameraInfos = result
                }
                CommonUtils.runOnUiThread(Runnable {
                    loadingCameras = false
                    cameraInitied = true
                    cameraInitCallback?.invoke()
                })
            } catch (e: Exception) {
                e.pst()
                loadingCameras = false
                cameraInitied = false
            }
        }
    }

    fun cleanup() {
        threadPool.execute {
            if (cameraInfos != null && cameraInfos!!.isNotEmpty()) {
                cameraInfos!!.filter { it.camera != null }
                        .forEach {
                            it.camera!!.stopPreview()
                            it.camera!!.setPreviewCallbackWithBuffer(null)
                            it.camera!!.release()
                            it.camera = null
                        }
            }
            cameraInfos = null
        }
    }

    fun close(session: CameraSession?, semaphore: Semaphore?, beforeDestroyRunnable: Runnable?) {
        if (session == null) {
            return
        }
        session.destroy()
        threadPool.execute {
            beforeDestroyRunnable?.run()
            session.cameraInfo.camera?.apply {
                try {
                    stopPreview()
                    setPreviewCallbackWithBuffer(null)
                } catch (e: Exception) {
                    e.pst()
                }

                try {
                    release()
                } catch (e: Exception) {
                    e.pst()
                }
            }
            session.cameraInfo.camera = null
            semaphore?.release()
        }
        if (semaphore != null) {
            try {
                semaphore.acquire()
            } catch (e: Exception) {
                e.pst()
            }

        }
    }

    private fun getOrientation(jpeg: ByteArray?): Int {
        if (jpeg == null) {
            return 0
        }

        var offset = 0
        var length = 0
        val FF: Byte = 0xFF.toByte()

        while (offset + 3 < jpeg.size && (jpeg[offset++] and FF) == FF) {
            val marker = jpeg[offset] and FF

            if (marker == FF) {
                continue
            }
            offset++

            if (marker == 0xD8.toByte() || marker == 0x01.toByte()) {
                continue
            }
            if (marker == 0xD9.toByte() || marker == 0xDA.toByte()) {
                break
            }

            length = pack(jpeg, offset, 2, false)
            if (length < 2 || offset + length > jpeg.size) {
                return 0
            }

            // Break if the marker is EXIF in APP1.
            if (marker == 0xE1.toByte() && length >= 8 &&
                    pack(jpeg, offset + 2, 4, false) == 0x45786966 &&
                    pack(jpeg, offset + 6, 2, false) == 0) {
                offset += 8
                length -= 8
                break
            }

            offset += length
            length = 0
        }

        if (length > 8) {
            var tag = pack(jpeg, offset, 4, false)
            if (tag != 0x49492A00 && tag != 0x4D4D002A) {
                return 0
            }
            val littleEndian = tag == 0x49492A00

            var count = pack(jpeg, offset + 4, 4, littleEndian) + 2
            if (count < 10 || count > length) {
                return 0
            }
            offset += count
            length -= count

            count = pack(jpeg, offset - 2, 2, littleEndian)
            while (count-- > 0 && length >= 12) {
                tag = pack(jpeg, offset, 2, littleEndian)
                if (tag == 0x0112) {
                    val orientation = pack(jpeg, offset + 8, 2, littleEndian)
                    when (orientation) {
                        1 -> return 0
                        3 -> return 180
                        6 -> return 90
                        8 -> return 270
                    }
                    return 0
                }
                offset += 12
                length -= 12
            }
        }
        return 0
    }

    private fun pack(bytes: ByteArray, offset: Int, length: Int, littleEndian: Boolean): Int {
        var offset = offset
        var length = length
        var step = 1
        if (littleEndian) {
            offset += length - 1
            step = -1
        }

        var value = 0
        while (length-- > 0) {
            value = ((value shl 8) or (bytes[offset] and 0xFF.toByte()).toInt())
            offset += step
        }
        return value
    }

    fun takePicture(path: File?, session: CameraSession?, callback: Runnable?): Boolean {
        if (session == null || path == null) {
            return false
        }
        val info = session.cameraInfo
        val camera = info.camera
        try {
            camera?.takePicture(null, null, Camera.PictureCallback { data, cam ->
                var bitmap: Bitmap? = null
                val size = (PHOTO_SIZE / CommonUtils.DENSITY).toInt()
                try {
                    val options = BitmapFactory.Options()
                    options.inJustDecodeBounds = true
                    BitmapFactory.decodeByteArray(data, 0, data.size, options)
                    var scaleFactor = Math.max(options.outWidth.toFloat() / PHOTO_SIZE, options.outHeight.toFloat() / PHOTO_SIZE)
                    if (scaleFactor < 1) {
                        scaleFactor = 1f
                    }
                    options.inJustDecodeBounds = false
                    options.inSampleSize = scaleFactor.toInt()
                    options.inPurgeable = true
                    bitmap = BitmapFactory.decodeByteArray(data, 0, data.size, options)
                } catch (e: Throwable) {
                    e.pst()
                }

                try {
                    if (info.frontCamera != 0 && bitmap != null) {
                        try {
                            val matrix = Matrix()
                            matrix.setRotate(getOrientation(data).toFloat())
                            matrix.postScale(-1f, 1f)
                            val scaled = Bitmaps.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, false)
                            bitmap.recycle()
                            val outputStream = FileOutputStream(path)
                            scaled.compress(Bitmap.CompressFormat.JPEG, 80, outputStream)
                            outputStream.flush()
                            outputStream.fd.sync()
                            outputStream.close()
                            callback?.run()
                            return@PictureCallback
                        } catch (e: Throwable) {
                            e.pst()
                        }

                    }
                    val outputStream = FileOutputStream(path)
                    outputStream.write(data)
                    outputStream.flush()
                    outputStream.fd.sync()
                    outputStream.close()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                callback?.run()
            })
            return true
        } catch (e: Exception) {
            e.pst()
        }
        return false
    }

    fun startPreview(session: CameraSession?) {
        if (session == null) {
            return
        }
        threadPool.execute {
            var camera = session.cameraInfo.camera
            try {
                if (camera == null) {
                    camera = Camera.open(session.cameraInfo.cameraId)
                    session.cameraInfo.camera = camera
                }
                camera!!.startPreview()
            } catch (e: Exception) {
                session.cameraInfo.camera = null
                if (camera != null) {
                    camera.release()
                }
                e.pst()
            }
        }
    }

    fun openRound(session: CameraSession?, texture: SurfaceTexture?, callback: Runnable?, configureCallback: Runnable?) {
        if (session == null || texture == null) {
            return
        }
        threadPool.execute {
            var camera = session.cameraInfo.camera
            try {
                if (camera == null) {
                    camera = Camera.open(session.cameraInfo.cameraId)
                    session.cameraInfo.camera = camera
                }
                session.configureRoundCamera()
                configureCallback?.run()
                camera!!.setPreviewTexture(texture)
                camera.startPreview()
                if (callback != null) {
                    CommonUtils.runOnUiThread(callback)
                }
            } catch (e: Exception) {
                session.cameraInfo.camera = null
                if (camera != null) {
                    camera.release()
                }
                e.pst()
            }
        }
    }

    fun open(session: CameraSession?, texture: SurfaceTexture?, callback: Runnable?, prestartCallback: Runnable?) {
        if (session == null || texture == null) {
            return
        }
        threadPool.execute {
            var camera = session.cameraInfo.camera
            try {
                if (camera == null) {
                    camera = Camera.open(session.cameraInfo.cameraId)
                    session.cameraInfo.camera = camera
                }
                val params = camera!!.parameters
                val rawFlashModes = params.supportedFlashModes

                availableFlashModes.clear()
                if (rawFlashModes != null) {
                    rawFlashModes
                            .filterTo(availableFlashModes) { it == Camera.Parameters.FLASH_MODE_OFF || it == Camera.Parameters.FLASH_MODE_ON || it == Camera.Parameters.FLASH_MODE_AUTO }
                    session.checkFlashMode(availableFlashModes[0])
                }

                prestartCallback?.run()

                session.configurePhotoCamera()
                camera.setPreviewTexture(texture)
                camera.startPreview()
                if (callback != null) {
                    CommonUtils.runOnUiThread(callback)
                }
            } catch (e: Exception) {
                session.cameraInfo.camera = null
                if (camera != null) {
                    camera.release()
                }
                e.pst()
            }
        }
    }

    fun recordVideo(session: CameraSession?, path: File?, callback: ((thumb: Bitmap?, recordedFile: File?) -> Unit)?, onVideoStartRecord: Runnable?, smallVideo: Boolean) {
        if (session == null || path == null) {
            return
        }

        val info = session.cameraInfo
        val camera = info.camera
        threadPool.execute {
            try {
                if (camera != null) {
                    try {
                        val params = camera.parameters
                        params.flashMode = if (session.currentFlashMode == Camera.Parameters.FLASH_MODE_ON) Camera.Parameters.FLASH_MODE_TORCH else Camera.Parameters.FLASH_MODE_OFF
                        camera.parameters = params
                    } catch (e: Exception) {
                        e.pst()
                    }

                    camera.unlock()
                    try {
                        recordingSmallVideo = smallVideo

                        recorder = MediaRecorder()
                        recorder?.apply {
                            setCamera(camera)
                            setVideoSource(MediaRecorder.VideoSource.CAMERA)
                            setAudioSource(MediaRecorder.AudioSource.CAMCORDER)
                            session.configureRecorder(1, recorder)
                            setOutputFile(path.absolutePath)
                            setMaxFileSize((1024 * 1024 * 1024).toLong())
                            setVideoFrameRate(30)
                            setMaxDuration(0)
                            var pictureSize: Size
                            if (recordingSmallVideo) {
                                pictureSize = Size(4, 3)
                                pictureSize = CameraController.chooseOptimalSize(info.pictureSizes, 640, 480, pictureSize)
                                setVideoEncodingBitRate(900000 * 2)
                                setAudioEncodingBitRate(32000)
                                setAudioChannels(1)
                            } else {
                                pictureSize = Size(16, 9)
                                pictureSize = CameraController.chooseOptimalSize(info.pictureSizes, 720, 480, pictureSize)
                                setVideoEncodingBitRate(900000 * 2)
                            }
                            setVideoSize(pictureSize.width, pictureSize.height)
                            setOnInfoListener(this@CameraControllerKt)
                            prepare()
                            start()
                        }


                        onVideoTakeCallback = callback
                        recordedFilePath = path.absolutePath
                        recordedFile = path
                        if (onVideoStartRecord != null) {
                            CommonUtils.runOnUiThread(onVideoStartRecord)
                        }
                    } catch (e: Exception) {
                        recorder?.release()
                        recorder = null
                        e.pst()
                    }
                }
            } catch (e: Exception) {
                e.pst()
            }
        }
    }

    override fun onInfo(mr: MediaRecorder?, what: Int, extra: Int) {
        if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED || what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_FILESIZE_REACHED || what == MediaRecorder.MEDIA_RECORDER_INFO_UNKNOWN) {
            val tempRecorder = recorder
            recorder = null
            if (tempRecorder != null) {
                tempRecorder.stop()
                tempRecorder.release()
            }
            if (onVideoTakeCallback != null) {
                val bitmap = ThumbnailUtils.createVideoThumbnail(recordedFilePath, MediaStore.Video.Thumbnails.MINI_KIND)
                CommonUtils.runOnUiThread(Runnable {
                    onVideoTakeCallback?.invoke(bitmap, recordedFile)
                    onVideoTakeCallback = null
                })
            }
        }
    }

    fun stopVideoRecording(session: CameraSession?, abandon: Boolean) {
        if (session == null) {
            return
        }
        threadPool.execute {
            try {
                val info = session.cameraInfo
                val camera = info.camera
                if (camera != null && recorder != null) {
                    val tempRecorder = recorder
                    recorder = null
                    try {
                        tempRecorder?.stop()
                    } catch (e: Exception) {
                        e.pst()
                    }

                    try {
                        tempRecorder?.release()
                    } catch (e: Exception) {
                        e.pst()
                    }

                    try {
                        camera.reconnect()
                        camera.startPreview()
                    } catch (e: Exception) {
                        e.pst()
                    }

                    try {
                        session.stopVideoRecording()
                    } catch (e: Exception) {
                        e.pst()
                    }
                }
                try {
                    camera?.apply {
                        val params = parameters
                        params.flashMode = Camera.Parameters.FLASH_MODE_OFF
                        parameters = params
                    }

                } catch (e: Exception) {
                    e.pst()
                }

                threadPool.execute {
                    try {
                        camera?.apply {
                            val params = parameters
                            params.flashMode = session.currentFlashMode
                            camera.parameters = params
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                if (!abandon && onVideoTakeCallback != null) {
                    val bitmap = if (!recordingSmallVideo) {
                        ThumbnailUtils.createVideoThumbnail(recordedFilePath, MediaStore.Video.Thumbnails.MINI_KIND)
                    } else null
                    CommonUtils.runOnUiThread(Runnable {
                        onVideoTakeCallback?.invoke(bitmap, recordedFile)
                        onVideoTakeCallback = null
                    })
                } else {
                    onVideoTakeCallback = null
                }
            } catch (e: Exception) {
                e.pst()
            }
        }
    }


    fun chooseOptimalSize(choices: List<Size>, width: Int, height: Int, aspectRatio: Size): Size {
        val w = aspectRatio.width
        val h = aspectRatio.height
        val bigEnough = choices
                .filter { it.height == it.width * h / w && it.width >= width && it.height >= height }
        return if (bigEnough.isNotEmpty()) {
            Collections.min(bigEnough, CompareSizesByArea())
        } else {
            Collections.max(choices, CompareSizesByArea())
        }
    }

    private class CompareSizesByArea : Comparator<Size> {
        override fun compare(lhs: Size, rhs: Size): Int {
            return java.lang.Long.signum(lhs.width.toLong() * lhs.height - rhs.width.toLong() * rhs.height)
        }
    }
}