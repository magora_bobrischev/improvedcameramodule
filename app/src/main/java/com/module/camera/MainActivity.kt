@file:Suppress("DEPRECATION")

package com.module.camera

import android.Manifest
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.hardware.Camera
import android.os.Build
import android.os.Bundle
import android.support.media.ExifInterface
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.util.TypedValue
import android.view.Gravity
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import java.io.File

class MainActivity : AppCompatActivity() {
    private val container: ViewGroup by lazy(LazyThreadSafetyMode.NONE) {
        findViewById<ViewGroup>(android.R.id.content)
    }
    private val flashModeButton by lazy(LazyThreadSafetyMode.NONE) {
        prepareFlashModeButtons()
    }
    private var cameraView: CameraView? = null
    private val shutterButton: ShutterButton by lazy(LazyThreadSafetyMode.NONE) {
        prepareShutterButton()
    }
    private val switchCameraButton: ImageView by lazy(LazyThreadSafetyMode.NONE) {
        prepareSwitchCameraButton()
    }
    private val recordTime: TextView by lazy {
        prepareRecordTimer()
    }
    private var cameraPanel: FrameLayout? = null
    private var paused = true
    private var takingPhoto = false
    private var mediaCaptured = false
    private var flashAnimationInProgress = false
    private var requestingPermissions = false
    private var cameraFile: File? = null
    private var videoRecordTime = 0
    private var videoRecordRunnable: Runnable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        CameraControllerKt.cameraInitCallback = {
            showCamera()
        }
        CameraControllerKt.initCamera()
    }


    private fun showCamera() {
        if (paused) {
            return
        }
        if (cameraView == null) {
            setupCameraView()
        }

        setupCameraControlPanel()
    }

    private fun setupCameraView() {
        cameraView = CameraView(this, false)
        cameraView?.apply {
            systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_FULLSCREEN
            setContentView(cameraView)
            setDelegate(object : CameraView.CameraViewDelegate {
                override fun onCameraCreated(camera: Camera) {

                }

                override fun onCameraInit() {
                    if (cameraSession.currentFlashMode == cameraSession.nextFlashMode) {
                        flashModeButton[0].visibility = View.INVISIBLE
                        flashModeButton[0].alpha = 0f
                        flashModeButton[0].translationY = 0f

                        flashModeButton[1].visibility = View.INVISIBLE
                        flashModeButton[1].alpha = 0f
                        flashModeButton[1].translationY = 0f
                    } else {
                        setCameraFlashModeIcon(flashModeButton[0], cameraSession.currentFlashMode)
                        flashModeButton[0].visibility = View.VISIBLE
                        flashModeButton[0].alpha = 1f
                        flashModeButton[0].translationY = 0f

                        flashModeButton[1].visibility = View.INVISIBLE
                        flashModeButton[1].alpha = 0f
                        flashModeButton[1].translationY = 0f
                    }
                    switchCameraButton.setImageResource(if (isFrontface) R.drawable.camera_revert1 else R.drawable.camera_revert2)
                    switchCameraButton.visibility = if (hasFrontFaceCamera()) View.VISIBLE else View.INVISIBLE
                    shutterButton.visibility = View.VISIBLE
                }
            })
            setOnTouchListener { _, event ->
                if (event.action == MotionEvent.ACTION_DOWN) {
                    focusToPoint(event.x.toInt(), event.y.toInt())
                }
                return@setOnTouchListener false
            }
        }
    }

    private fun prepareRecordTimer(): TextView {
        val result = TextView(this)
        result.setBackgroundResource(R.drawable.bg_record_timer)
        result.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16f)
        result.alpha = 0f
        result.setTextColor(Color.WHITE)
        val padding1 = CommonUtils.dp(10)
        val padding2 = CommonUtils.dp(5)
        result.setPadding(padding1, padding2, padding1, padding2)
        return result
    }

    private fun prepareShutterButton(): ShutterButton {
        val btn = ShutterButton(this)
        btn.visibility = View.INVISIBLE
        btn.delegate = object : ShutterButton.ShutterButtonDelegate {

            override fun shutterLongPressed(): Boolean {
                return recordVideo()
            }

            override fun shutterReleased() {
                onShutterReleased()
            }

            override fun shutterCancel() {
                onShutterCancelled()
            }
        }
        return btn
    }

    private fun recordVideo(): Boolean {
        if (mediaCaptured || takingPhoto || cameraView == null) {
            return false
        }
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                requestingPermissions = true
                requestPermissions(arrayOf(Manifest.permission.RECORD_AUDIO), 21)
                return false
            }
        }
        flashModeButton[0].alpha = 0.0f
        flashModeButton[1].alpha = 0.0f
        switchCameraButton.alpha = 0.0f
        cameraFile = CommonUtils.generateVideoPath(applicationContext)
        recordTime.alpha = 1.0f
        recordTime.text = String.format("%02d:%02d", 0, 0)
        videoRecordTime = 0
        videoRecordRunnable = Runnable {
            if (videoRecordRunnable == null) {
                return@Runnable
            }
            videoRecordTime++
            recordTime.text = String.format("%02d:%02d", videoRecordTime / 60, videoRecordTime % 60)
            CommonUtils.runOnUiThread(videoRecordRunnable!!, 1000)
        }
        CommonUtils.lockOrientation(this)
        CameraControllerKt.recordVideo(cameraView!!.cameraSession, cameraFile, { bitmap: Bitmap?, file: File? ->
            Log.i("M_DATA", "thumb: $bitmap file $file")
        }, Runnable {
            if (videoRecordRunnable != null) {
                CommonUtils.runOnUiThread(videoRecordRunnable!!)
            }
        }, false)

        shutterButton.setState(ShutterButton.State.RECORDING, true)
        return true
    }

    private fun onShutterReleased() {
        if (takingPhoto || cameraView == null || mediaCaptured || cameraView?.cameraSession == null) {
            return
        }
        mediaCaptured = true
        val cv = cameraView!!
        if (shutterButton.state === ShutterButton.State.RECORDING) {
            resetRecordState()
            CameraControllerKt.stopVideoRecording(cv.cameraSession, false)
            shutterButton.setState(ShutterButton.State.DEFAULT, true)
            return
        }
        cameraFile = CommonUtils.generatePicturePath(applicationContext)
        val sameTakePictureOrientation = cv.cameraSession.isSameTakePictureOrientation
        takingPhoto = CameraControllerKt.takePicture(cameraFile, cv.cameraSession, Runnable {
            takingPhoto = false
            if (cameraFile == null) {
                return@Runnable
            }
            var orientation = 0
            try {
                val ei = ExifInterface(cameraFile!!.absolutePath)
                val exif = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
                when (exif) {
                    ExifInterface.ORIENTATION_ROTATE_90 -> orientation = 90
                    ExifInterface.ORIENTATION_ROTATE_180 -> orientation = 180
                    ExifInterface.ORIENTATION_ROTATE_270 -> orientation = 270
                }
                onPictureTaken(cameraFile!!, orientation)
            } catch (e: Exception) {

            }
        })
    }

    private fun onPictureTaken(path: File, orientation: Int) {

    }

    private fun onShutterCancelled() {
        if (mediaCaptured) {
            return
        }
        cameraFile?.delete()
        resetRecordState()
        CameraControllerKt.stopVideoRecording(cameraView?.cameraSession, true)
    }

    private fun resetRecordState() {
        flashModeButton[0].alpha = 1.0f
        flashModeButton[1].alpha = 1.0f
        switchCameraButton.alpha = 1.0f

        recordTime.alpha = 0.0f
        CommonUtils.cancelRunOnUiThread(videoRecordRunnable)
        videoRecordRunnable = null
        CommonUtils.unlockOrientation(this)
    }

    private fun prepareSwitchCameraButton(): ImageView {
        val btn = ImageView(this)
        btn.visibility = View.INVISIBLE
        btn.scaleType = ImageView.ScaleType.CENTER
        btn.setOnClickListener {
            onSwitchCameraButtonClicked(it as ImageView)
        }
        return btn
    }

    private fun onSwitchCameraButtonClicked(btn: ImageView) {
        if (takingPhoto || cameraView == null || !cameraView!!.isInitied) {
            return
        }

        cameraView!!.switchCamera()

        btn.animate().cancel()
        btn.animate().scaleX(0f)
                .setDuration(100)
                .withEndAction {
                    if (cameraView != null) {
                        val resId = if (cameraView!!.isFrontface) R.drawable.camera_revert1 else R.drawable.camera_revert2
                        btn.setImageResource(resId)
                        btn.animate().scaleX(1f).duration = 100
                    }
                }
    }

    private fun setupCameraControlPanel() {
        cameraPanel = object : FrameLayout(this) {

            override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
                var cx = measuredWidth / 2
                var cy = measuredHeight / 2
                var cx2: Int
                var cy2: Int
                shutterButton.layoutAround(cx, cy)
                if (measuredWidth == CommonUtils.dp(100F)) {
                    cx2 = measuredWidth / 2
                    cx = cx2
                    cy2 = cy + cy / 2 + CommonUtils.dp(17F)
                    cy = cy / 2 - CommonUtils.dp(17F)
                } else {
                    cx2 = cx + cx / 2 + CommonUtils.dp(17F)
                    cx = cx / 2 - CommonUtils.dp(17F)
                    cy2 = measuredHeight / 2
                    cy = cy2
                }
                switchCameraButton.layoutAround(cx2, cy2)
                flashModeButton[0].layoutAround(cx, cy)
                flashModeButton[1].layoutAround(cx, cy)
            }
        }
        cameraPanel!!.apply {
            addView(shutterButton, frameParams(84, 84, Gravity.CENTER))
            addView(switchCameraButton, frameParams(48, 48, Gravity.END or Gravity.CENTER_VERTICAL))
            addView(flashModeButton[0], frameParams(48, 48, Gravity.START or Gravity.TOP))
            addView(flashModeButton[1], frameParams(48, 48, Gravity.START or Gravity.TOP))
        }
        container.addView(recordTime, frameParams(WRAP_CONTENT, WRAP_CONTENT, Gravity.CENTER_HORIZONTAL or Gravity.TOP, 0, 16, 0, 0))
        container.addView(cameraPanel, frameParams(MATCH_PARENT, 100, Gravity.BOTTOM))
    }

    private fun prepareFlashModeButtons(): Array<ImageView> {
        val btn0 = ImageView(this)
        btn0.scaleType = ImageView.ScaleType.CENTER
        btn0.visibility = View.INVISIBLE
        btn0.setOnClickListener {
            onFlashModeButtonClicked(it as ImageView)
        }
        val btn1 = ImageView(this)
        btn1.scaleType = ImageView.ScaleType.CENTER
        btn1.visibility = View.INVISIBLE
        btn1.setOnClickListener {
            onFlashModeButtonClicked(it as ImageView)
        }
        return arrayOf(btn0, btn1)
    }

    private fun onFlashModeButtonClicked(currentImage: ImageView) {
        if (flashAnimationInProgress || cameraView == null || !cameraView!!.isInitied) {
            return
        }
        val cv = cameraView!!
        val current = cv.cameraSession.currentFlashMode
        val next = cv.cameraSession.nextFlashMode
        if (current == next) {
            return
        }
        cv.cameraSession.currentFlashMode = next
        flashAnimationInProgress = true
        val nextImage = if (flashModeButton[0] === currentImage) flashModeButton[1] else flashModeButton[0]
        nextImage.visibility = View.VISIBLE
        setCameraFlashModeIcon(nextImage, next)
        val animatorSet = AnimatorSet()
        animatorSet.playTogether(
                ObjectAnimator.ofFloat(currentImage, View.TRANSLATION_Y, 0f, CommonUtils.dp(48).toFloat()),
                ObjectAnimator.ofFloat(nextImage, View.TRANSLATION_Y, -CommonUtils.dp(48).toFloat(), 0F),
                ObjectAnimator.ofFloat(currentImage, View.ALPHA, 1.0f, 0.0f),
                ObjectAnimator.ofFloat(nextImage, View.ALPHA, 0.0f, 1.0f)
        )
        animatorSet.duration = 200
        animatorSet.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animator: Animator) {
                flashAnimationInProgress = false
                currentImage.visibility = View.INVISIBLE
            }
        })
        animatorSet.start()
    }

    private fun setCameraFlashModeIcon(imageView: ImageView, mode: String) {
        when (mode) {
            Camera.Parameters.FLASH_MODE_OFF -> imageView.setImageResource(R.drawable.flash_off)
            Camera.Parameters.FLASH_MODE_ON -> imageView.setImageResource(R.drawable.flash_on)
            Camera.Parameters.FLASH_MODE_AUTO -> imageView.setImageResource(R.drawable.flash_auto)
        }
    }

    private fun hideCamera(async: Boolean) {
        cameraView?.apply {
            destroy(async, null)
        }
        container.removeAllViews()
        cameraPanel?.removeAllViews()
        cameraView = null
        cameraPanel = null
    }

    override fun onResume() {
        super.onResume()
        paused = false
        if (CameraControllerKt.isCameraInited) {
            showCamera()
        }
    }

    override fun onPause() {
        hideCamera(true)
        paused = true
        super.onPause()
    }
}
