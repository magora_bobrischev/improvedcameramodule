package com.module.camera

import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 18.02.18.
 */

const val MATCH_PARENT = ViewGroup.LayoutParams.MATCH_PARENT
const val WRAP_CONTENT = ViewGroup.LayoutParams.WRAP_CONTENT

fun frameParams(w: Int, h: Int) = FrameLayout.LayoutParams(getSize(w), getSize(h))

fun frameParams(w: Int, h: Int, gravity: Int) = FrameLayout.LayoutParams(getSize(w), getSize(h), gravity)

fun frameParams(w: Int, h: Int, gravity: Int, marginLeft: Int, marginTop: Int, marginRight: Int, marginBottom: Int): FrameLayout.LayoutParams {
    val params = FrameLayout.LayoutParams(getSize(w), getSize(h), gravity)
    params.setMargins(getSize(marginLeft), getSize(marginTop), getSize(marginRight), getSize(marginBottom))
    return params
}

fun View.layoutAround(cx: Int, cy: Int) {
    layout(cx - measuredWidth / 2, cy - measuredHeight / 2, cx + measuredWidth / 2, cy + measuredHeight / 2)
}

private fun getSize(size: Int): Int {
    return if (size > 0) CommonUtils.dp(size) else size
}

fun Throwable?.pst() {
    if (this != null && BuildConfig.DEBUG) {
        printStackTrace()
    }
}