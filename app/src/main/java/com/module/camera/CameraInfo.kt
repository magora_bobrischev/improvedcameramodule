package com.module.camera

import android.hardware.Camera
import java.util.*

@Suppress("DEPRECATION")
class CameraInfo(var cameraId: Int, info: Camera.CameraInfo) {
    var camera: Camera? = null
    var pictureSizes = ArrayList<Size>()
    var previewSizes = ArrayList<Size>()
    val frontCamera: Int = info.facing
    val isFrontface: Boolean
        get() = frontCamera != 0
}
