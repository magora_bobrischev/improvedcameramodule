package com.module.camera

import android.app.Activity
import android.content.Context
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.content.res.Resources
import android.graphics.Point
import android.os.Build
import android.os.Environment
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.Surface
import android.view.WindowManager
import java.io.File
import java.security.SecureRandom
import java.text.SimpleDateFormat
import java.util.*
import kotlin.experimental.and

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 18.02.18.
 */
class CommonUtils {
    companion object {
        val DENSITY: Float by lazy {
            Resources.getSystem().displayMetrics.density
        }
        val DISPLAY_SIZE: Point by lazy {
            Point(Resources.getSystem().displayMetrics.widthPixels, Resources.getSystem().displayMetrics.heightPixels)
        }

        private val handler = Handler(Looper.getMainLooper())

        fun runOnUiThread(run: Runnable) {
            runOnUiThread(run, 0L)
        }

        fun runOnUiThread(run: Runnable, delay: Long) {
            if (delay == 0L) {
                if (Looper.getMainLooper() == Looper.myLooper()) {
                    run.run()
                } else {
                    handler.post(run)
                }
            } else {
                handler.postDelayed(run, delay)
            }
        }

        fun cancelRunOnUiThread(run: Runnable?) {
            handler.removeCallbacks(run)
        }

        fun MD5(md5: String?): String? {
            if (md5 == null) {
                return null
            }
            try {
                val md = java.security.MessageDigest.getInstance("MD5")
                val array = md.digest(md5.toByteArray())
                val sb = StringBuilder()
                for (a in array.indices) {
                    sb.append(Integer.toHexString((array[a] and 0xFF.toByte()).toInt() or 0x100).substring(1, 3))
                }
                return sb.toString()
            } catch (e: java.security.NoSuchAlgorithmException) {
                e.printStackTrace()
            }

            return null
        }

        fun dp(value: Float): Int {
            return if (value == 0f) {
                0
            } else Math.ceil((DENSITY * value).toDouble()).toInt()
        }


        fun dp(value: Int): Int {
            return if (value == 0) {
                0
            } else Math.ceil((DENSITY * value).toDouble()).toInt()
        }

        fun dp2(value: Float): Int {
            return if (value == 0f) {
                0
            } else Math.floor((DENSITY * value).toDouble()).toInt()
        }

        fun compare(lhs: Int, rhs: Int): Int {
            if (lhs == rhs) {
                return 0
            } else if (lhs > rhs) {
                return 1
            }
            return -1
        }

        fun dpf2(value: Float): Float {
            return if (value == 0f) {
                0f
            } else DENSITY * value
        }

        private var prevOrientation: Int = -10

        fun lockOrientation(activity: Activity?) {
            if (activity == null || prevOrientation != -10) {
                return
            }
            try {
                prevOrientation = activity.requestedOrientation
                val manager = activity.getSystemService(Activity.WINDOW_SERVICE) as WindowManager
                if (manager.defaultDisplay != null) {
                    val rotation = manager.defaultDisplay.rotation
                    val orientation = activity.resources.configuration.orientation

                    if (rotation == Surface.ROTATION_270) {
                        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                            activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                        } else {
                            activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE
                        }
                    } else if (rotation == Surface.ROTATION_90) {
                        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                            activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT
                        } else {
                            activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                        }
                    } else if (rotation == Surface.ROTATION_0) {
                        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                            activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                        } else {
                            activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                        }
                    } else {
                        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                            activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE
                        } else {
                            activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT
                        }
                    }
                }
            } catch (ignored: Exception) {
                ignored.pst()
            }

        }

        fun unlockOrientation(activity: Activity?) {
            if (activity == null) {
                return
            }
            try {
                if (prevOrientation != -10) {
                    activity.requestedOrientation = prevOrientation
                    prevOrientation = -10
                }
            } catch (ignored: Exception) {
                ignored.pst()
            }

        }

        private val random = SecureRandom()

        fun generatePicturePath(appContext: Context): File? {
            try {
                val storageDir = getAlbumDir(appContext)
                val date = Date()
                date.time = System.currentTimeMillis() + random.nextInt(1000) + 1
                val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss_SSS", Locale.US).format(date)
                return File(storageDir, "IMG_$timeStamp.jpg")
            } catch (ignored: Exception) {
                ignored.pst()
            }
            return null
        }

        fun generateVideoPath(appContext: Context): File? {
            try {
                val storageDir = getAlbumDir(appContext)
                val date = Date()
                date.time = System.currentTimeMillis() + random.nextInt(1000) + 1
                val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss_SSS", Locale.US).format(date)
                return File(storageDir, "VID_$timeStamp.mp4")
            } catch (ignored: Exception) {
                ignored.pst()
            }

            return null
        }

        private fun getAlbumDir(appContext: Context): File? {
            if (Build.VERSION.SDK_INT >= 23 && appContext.checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                val f = File(Environment.DIRECTORY_PICTURES, appContext.getString(R.string.app_name))
                if (!f.exists()) {
                    f.mkdir()
                }
                return f
            }
            var storageDir: File? = null
            if (Environment.MEDIA_MOUNTED == Environment.getExternalStorageState()) {
                storageDir = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), appContext.getString(R.string.app_name))
                if (!storageDir.mkdirs()) {
                    if (!storageDir.exists()) {
                        return null
                    }
                }
            }

            return storageDir
        }
    }
}