/*
 * This is the source code of Telegram for Android v. 3.x.x.
 * It is licensed under GNU GPL v. 2 or later.
 * You should have received a copy of the license in this archive (see LICENSE).
 *
 * Copyright Nikolai Kudashov, 2013-2017.
 */

package com.module.camera

data class Size(val width: Int, val height: Int) : Comparable<Size> {

    override fun compareTo(other: Size): Int {
        if (width < other.width) {
            return -1
        } else if (width > other.width) {
            return 1
        }
        return 0
    }

    override fun toString(): String {
        return width.toString() + "x" + height
    }

    companion object {

        private fun invalidSize(s: String): NumberFormatException {
            throw NumberFormatException("Invalid Size: \"" + s + "\"")
        }

        @Throws(NumberFormatException::class)
        fun parseSize(string: String): Size {
            var sep_ix = string.indexOf('*')
            if (sep_ix < 0) {
                sep_ix = string.indexOf('x')
            }
            if (sep_ix < 0) {
                throw invalidSize(string)
            }
            try {
                return Size(Integer.parseInt(string.substring(0, sep_ix)), Integer.parseInt(string.substring(sep_ix + 1)))
            } catch (e: NumberFormatException) {
                throw invalidSize(string)
            }
        }
    }
}
